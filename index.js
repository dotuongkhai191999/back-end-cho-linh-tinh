var express = require("express");
var app = express();
var server = require("http").Server(app);
const {
    test
} = require("./events/chat");


// app.use(cors());
// server.listen(5000);
server.listen(process.env.PORT || 5000, () => {
    console.log(`app is running on port ${process.env.PORT}`);
});
app.set('port', process.env.PORT || 3001);
// enable CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function (req, res) {
    const usr = test();
    console.log(usr)
    res.json(usr);
})
